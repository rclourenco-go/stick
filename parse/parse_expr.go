package parse

import "fmt"
import "container/list"

const exprInner       = 0
const exprTestOperand = 1
// parseExpr parses an expression.
func (t *Tree) parseExpr() (Expr, error) {
	elist := list.New()

	mode := exprInner
	for {
		var expr Expr
		var err error

		if mode == exprInner {
			expr, err = t.parseInnerExpr()
		} else {
			expr, err = t.parseRightTestOperand(nil)
		}
		if err != nil {
			return nil, err
		}

		oexpr, err := t.completeExpr(expr, elist)
		if err != nil {
			return nil, err
		}

		elist.PushBack(oexpr)

		nt := t.nextNonSpace()
		if nt.tokenType == tokenOperator {
			op, ok := binaryOperators[nt.value]
			if !ok {
				return nil, newUnexpectedTokenError(nt)
			}

			if op.op == OpBinaryIs || op.op == OpBinaryIsNot {
				mode = exprTestOperand
			} else {
				mode = exprInner
			}
			elist.PushBack(op)
		} else {
			t.backup()
			break
		}
	}

	expr, err := parseOperationList(elist)
	return expr, err
}

func (t *Tree) parseFilterExpr() (Expr, error) {
	expr, err := t.parseInnerExpr()
	if err != nil {
		return nil, err
	}

	oexpr, err := t.completeExpr(expr, nil)

	return oexpr, err
}

func (t *Tree) completeExpr(expr Expr, elist *list.List) (Expr, error) {
	for {
		switch nt := t.nextNonSpace(); nt.tokenType {
		case tokenParensOpen:
			switch name := expr.(type) {
			case *NameExpr:
				// TODO: This duplicates some code in parseInnerExpr, are both necessary?
				return t.parseFunc(name)
			default:
				return nil, newUnexpectedTokenError(nt)
			}

		case tokenArrayOpen, tokenPunctuation:
			switch nt.value {
			case "[":
				var args = make([]Expr, 0)
		//		attr, err := t.parseInnerExpr()
				attr, err := t.parseExpr()
				if err != nil {
					return nil, err
				}
				switch attr.(type) {
				case *NameExpr, *StringExpr, *NumberExpr, *GroupExpr, *GetAttrExpr:
					// valid
				default:
					return nil, newUnexpectedTokenError(nt)
				}

				_, err = t.expect(tokenArrayClose)
				if err != nil {
					return nil, err
				}
				expr = NewGetAttrExpr(expr, attr, args, nt.Pos)

			case ".": // Dot or array access
				var args = make([]Expr, 0)
				attr, err := t.parseInnerExpr()
				if err != nil {
					return nil, err
				}

				switch exp := attr.(type) {
				case *NameExpr:
					// valid, but we want to treat the name as a string
					attr = NewStringExpr(exp.Name, exp.Pos)
				case *NumberExpr:
					// Compatibility with Twig: {{ val.0 }}
					attr = NewStringExpr(exp.Value, exp.Pos)
				case *FuncExpr:
					// method call
					for _, v := range exp.Args {
						args = append(args, v)
					}
					attr = NewStringExpr(exp.Name, exp.Pos)
				default:
					return nil, newUnexpectedTokenError(nt)
				}

				expr = NewGetAttrExpr(expr, attr, args, nt.Pos)

			case "|": // Filter application
				nx, err := t.parseFilterExpr()
				if err != nil {
					return nil, err
				}
				switch n := nx.(type) {
				case *NameExpr:
					return NewFilterExpr(n.Name, []Expr{expr}, nt.Pos), nil

				case *FuncExpr:
					n.Args = append([]Expr{expr}, n.Args...)
					return NewFilterExpr(n.Name, n.Args, n.Pos), nil
				case *FilterExpr:
					n.Args = append([]Expr{expr}, n.Args...)
					return NewFilterExpr(n.Name, n.Args, n.Pos), nil
				default:
					return nil, newUnexpectedTokenError(nt)
				}

			case "?": // Ternary if
				if elist != nil {
					var err error
					elist.PushBack(expr)
					expr, err = parseOperationList(elist)
					if err != nil {
						return nil, err
					}
				}

				tx, err := t.parseExpr()
				if err != nil {
					return nil, err
				}
				_, err = t.expectValue(tokenPunctuation, ":")
				if err != nil {
					return nil, err
				}
				fx, err := t.parseExpr()
				if err != nil {
					return nil, err
				}
				return NewTernaryIfExpr(expr, tx, fx, expr.Start()), nil

			default:
				t.backup()
				return expr, nil
			}

		default:
			t.backup()
			return expr, nil
		}
	}
}

// parseIsRightOperand handles "is" and "is not" tests, which can
// themselves be two words, such as "divisible by":
//	{% if 10 is divisible by(3) %}
func (t *Tree) parseRightTestOperand(prev *NameExpr) (*TestExpr, error) {
	right, err := t.parseInnerExpr()
	if err != nil {
		return nil, err
	}
	if prev == nil {
		if r, ok := right.(*NameExpr); ok {
			if nxt := t.peekNonSpace(); nxt.tokenType == tokenName {
				return t.parseRightTestOperand(r)
			}
		}
	}
	switch r := right.(type) {
	case *NameExpr:
		if prev != nil {
			r.Name = prev.Name + " " + r.Name
		}
		return NewTestExpr(r.Name, []Expr{}, r.Pos), nil

	case *FuncExpr:
		if prev != nil {
			r.Name = prev.Name + " " + r.Name
		}
		return &TestExpr{r}, nil
	case *NullExpr:
		return NewTestExpr("null", []Expr{}, r.Pos), nil
	default:
		return nil, fmt.Errorf(`Expected name or function, got "%v"`, right)
	}
}

// parseInnerExpr attempts to parse an inner expression.
// An inner expression is defined as a cohesive expression, such as a literal.
func (t *Tree) parseInnerExpr() (Expr, error) {
	switch tok := t.nextNonSpace(); tok.tokenType {
	case tokenEOF:
		return nil, newUnexpectedEOFError(tok)

	case tokenOperator:
		op, ok := unaryOperators[tok.value]
		if !ok {
			return nil, newUnexpectedTokenError(tok)
		}
		expr, err := t.parseExpr()
		if err != nil {
			return nil, err
		}
		return NewUnaryExpr(op.Operator(), expr, tok.Pos), nil

	case tokenParensOpen:
		inner, err := t.parseExpr()
		if err != nil {
			return nil, err
		}
		_, err = t.expect(tokenParensClose)
		if err != nil {
			return nil, err
		}
		return NewGroupExpr(inner, tok.Pos), nil

	case tokenHashOpen:
		els := []*KeyValueExpr{}
		for {
			nxt := t.peek()
			if nxt.tokenType == tokenHashClose {
				t.next()
				break
			}
			keyExpr, err := t.parseExpr()
			if err != nil {
				return nil, err
			}
			_, err = t.expectValue(tokenPunctuation, delimHashKeyValue)
			if err != nil {
				return nil, err
			}
			valExpr, err := t.parseExpr()
			if err != nil {
				return nil, err
			}
			els = append(els, NewKeyValueExpr(keyExpr, valExpr, nxt.Pos))
			nxt = t.peek()
			if nxt.tokenType == tokenPunctuation {
				_, err := t.expectValue(tokenPunctuation, ",")
				if err != nil {
					return nil, err
				}
			}
		}
		return NewHashExpr(tok.Pos, els...), nil

	case tokenArrayOpen:
		els := []Expr{}
		for {
			nxt := t.peek()
			if nxt.tokenType == tokenArrayClose {
				t.next()
				break
			}
			expr, err := t.parseExpr()
			if err != nil {
				return nil, err
			}
			els = append(els, expr)
			nxt = t.peek()
			if nxt.tokenType == tokenPunctuation {
				_, err := t.expectValue(tokenPunctuation, ",")
				if err != nil {
					return nil, err
				}
			}
		}
		return NewArrayExpr(tok.Pos, els...), nil

	case tokenNumber:
		nxt := t.peek()
		val := tok.value
		if nxt.tokenType == tokenPunctuation && nxt.value == "." {
			val = val + "."
			t.next()
			nxt, err := t.expect(tokenNumber)
			if err != nil {
				return nil, err
			}
			val = val + nxt.value
		}
		return NewNumberExpr(val, tok.Pos), nil

	case tokenName:
		switch tok.value {
		case "null", "NULL", "none", "NONE":
			return NewNullExpr(tok.Pos), nil
		case "true", "TRUE":
			return NewBoolExpr(true, tok.Pos), nil
		case "false", "FALSE":
			return NewBoolExpr(false, tok.Pos), nil
		}
		name := NewNameExpr(tok.value, tok.Pos)
		nt := t.nextNonSpace()
		if nt.tokenType == tokenParensOpen {
			// TODO: This duplicates some code in parseOuterExpr, are both necessary?
			return t.parseFunc(name)
		}
		t.backup()
		return name, nil

	case tokenStringOpen:
		var exprs []Expr
		for {
			nxt, err := t.expect(tokenText, tokenInterpolateOpen, tokenStringClose)
			if err != nil {
				return nil, err
			}
			switch nxt.tokenType {
			case tokenText:
				exprs = append(exprs, NewStringExpr(nxt.value, nxt.Pos))
			case tokenInterpolateOpen:
				exp, err := t.parseExpr()
				if err != nil {
					return nil, err
				}
				_, err = t.expect(tokenInterpolateClose)
				if err != nil {
					return nil, err
				}
				exprs = append(exprs, exp)
			case tokenStringClose:
				ln := len(exprs)
				if ln > 1 {
					var res *BinaryExpr
					for i := 1; i < ln; i++ {
						if res == nil {
							res = NewBinaryExpr(exprs[i-1], OpBinaryConcat, exprs[i], exprs[i-1].Start())
							continue
						}
						res = NewBinaryExpr(res, OpBinaryConcat, exprs[i], res.Pos)
					}
					return res, nil
				}
				return exprs[0], nil
			}
		}

	default:
		return nil, newUnexpectedTokenError(tok)
	}
}

// parseFunc parses a function call expression from the first argument expression until the closing parenthesis.
func (t *Tree) parseFunc(name *NameExpr) (Expr, error) {
	var args []Expr
	for {
		switch tok := t.peek(); tok.tokenType {
		case tokenEOF:
			return nil, newUnexpectedEOFError(tok)

		case tokenParensClose:
		// do nothing

		default:
			argexp, err := t.parseExpr()
			if err != nil {
				return nil, err
			}

			args = append(args, argexp)
		}

		switch tok := t.nextNonSpace(); tok.tokenType {
		case tokenEOF:
			return nil, newUnexpectedEOFError(tok)

		case tokenPunctuation:
			if tok.value != "," {
				return nil, newUnexpectedValueError(tok, ",")
			}

		case tokenParensClose:
			return NewFuncExpr(name.Name, args, name.Pos), nil

		default:
			return nil, newUnexpectedTokenError(tok, tokenPunctuation, tokenParensClose)
		}
	}
}

func parseOperationList(elist *list.List) (Expr, error) {

	// first, we try the simple cases

	if elist == nil || elist.Len() == 0 {

		return nil, nil

	} else if elist.Len() == 1 {

		e1 := elist.Front()
		if e1 == nil {
			return nil, fmt.Errorf("Error at rpn")
		}
		expr := e1.Value.(Expr)
		elist.Init()
		return expr.(Expr), nil

	} else if elist.Len() == 3 {

		e1 := elist.Front()
		if e1 == nil {
			return nil, fmt.Errorf("Error at rpn")
		}
		e2 := e1.Next()
		if e2 == nil {
			return nil, fmt.Errorf("Error at rpn")
		}
		e3 := e2.Next()
		if e3 == nil {
			return nil, fmt.Errorf("Error at rpn")
		}
		expr  := e1.Value.(Expr)
		right := e3.Value.(Expr)
		op    := e2.Value.(operator)
		r := NewBinaryExpr(expr, op.Operator(), right, expr.Start())

		elist.Init()
		return r, nil

	}

	// then, deal with more complex expressions
	return parseRpnList(elist)
}

func getOperator(e *list.Element) (operator, error) {
	var op operator

	if e == nil {
		return op, fmt.Errorf("Rpn error")
	}

	switch e.Value.(type) {
	case operator:
		op = e.Value.(operator)
		return op, nil
	}

	return op, fmt.Errorf("No operator")
}


func popExpression(l *list.List) (Expr, error) {
	e := l.Back()
	if e == nil {
		return nil, fmt.Errorf("Empty rpn stack")
	}
	l.Remove(e)

	switch e.Value.(type) {
	case Expr:
		return e.Value.(Expr), nil
	}

	return nil, fmt.Errorf("No expression")
}


func parseRpnList(elist *list.List) (Expr, error) {

	stk  := list.New()
	wstk := list.New()

	for e := elist.Front(); e != nil; e = e.Next() {
		v := e.Value
		switch v.(type) {
		case Expr:
			stk.PushBack(v)
		case operator:
			top := wstk.Back()
			for top != nil {
				op1 := v.(operator)
				op2, err := getOperator(top)
				if err != nil {
					return nil, err
				}
				if op1.precedence < op2.precedence || (op1.precedence == op2.precedence && op1.leftAssoc()) {
					stk.PushBack(op2)
					wstk.Remove(top)
				} else {
					break
				}
				top = wstk.Back()
			}
			wstk.PushBack(v)
		}
	}

	for wstk.Len() > 0 {
		e := wstk.Back()
		if e != nil {
			wstk.Remove(e)
			stk.PushBack(e.Value)
		} else {
			return nil, fmt.Errorf("Rpn error")
		}
	}

	for e := stk.Front(); e != nil; e = e.Next() {
		v := e.Value
		switch v.(type) {
		case Expr:
			wstk.PushBack(v)
		case operator:
			op := v.(operator)
			if op.unary { // not expected
				v1, err := popExpression(wstk)

				if err != nil || v1 == nil {
					return nil, err
				}

				r  := NewUnaryExpr(op.Operator(), v1, v1.Start())
				wstk.PushBack(r)

			} else {
				v2, err := popExpression(wstk)

				if err != nil || v2 == nil {
					return nil, err
				}

				v1, err := popExpression(wstk)

				if err != nil || v1 == nil {
					return nil, err
				}

				r  := NewBinaryExpr(v1, op.Operator(), v2, v1.Start())
				wstk.PushBack(r)
			}
		}
	}

	if wstk.Len() != 1 {
		return nil, fmt.Errorf("Rpn error")
	}
	expr, err := popExpression(wstk)
	if err != nil {
		return nil, err
	}

	elist.Init()
	return expr, nil
}


