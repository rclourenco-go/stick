package main

import "bytes"
import "fmt"
import "unicode/utf8"

type TwigToken struct {
	t     byte
	value interface{}
}

type TwigReader struct {
	content  *bytes.Reader
	ordering bool
	ordertag string
	pos      int
	text     bytes.Buffer
	tl       []*TwigToken
	stash    map[string]string
}

func NewToken(t byte, value interface{}) *TwigToken {
	return &TwigToken{t: t, value: value}
}

func (this *TwigReader) PushText() {
	this.tl = append(this.tl, NewToken('t', this.text.String()))
	this.text.Reset()
}

func (this *TwigReader) PushVariable(text string) {
	this.tl = append(this.tl, NewToken('v', text))
}

func isLimiter(c rune) bool {
	switch c {
	case '{', '}', '[', ']', ',', ':':
		return true
	default:
		return false
	}
}

func (this *TwigReader) NextChar() (rune, error) {
	r, c, e := this.content.ReadRune()
	this.pos += c
	return r, e
}

func (this *TwigReader) BackChar(k rune) {
	this.pos -= utf8.RuneLen(k)
	this.content.UnreadRune()
}

func (this *TwigReader) ParseTemplate() {
	var k, nk rune
	var err error
	for k, err = this.NextChar(); err == nil; k, err = this.NextChar() {
		if k == '{' {
			nk, err = this.NextChar()
			if err != nil {
				break
			}
			switch nk {
			case '%':
				this.PushText()
				this.ParseTag()
			case '{':
				this.PushText()
				this.ParseVar()
			default:
				this.BackChar(nk)
				this.text.WriteRune(k)
			}
		} else {
			this.text.WriteRune(k)
		}
	}
	this.PushText()
}

func (this *TwigReader) ParseVar() {
	var k, nk rune
	var err error
	for k, err = this.NextChar(); err == nil; k, err = this.NextChar() {
		if k == '}' {
			nk, err = this.NextChar()
			if err != nil {
				break
			}
			if nk == '}' {
				break
			}
		} else if k == '"' || k == '\'' {
			// parse string
		} else if k != ' ' {
			this.ParseName(k)
		}
	}
}

func (this *TwigReader) ParseName(f rune) {
	var name bytes.Buffer
	name.WriteRune(f)
	for k, err := this.NextChar(); err == nil; k, err = this.NextChar() {
		if k != ' ' && k != '%' && k != '}' {
			name.WriteRune(k)
		} else {
			this.BackChar(k)
			break
		}
	}
	this.PushVariable(name.String())
}

func (this *TwigReader) ParseTag() {
	var k, nk rune
	var err error
	for k, err = this.NextChar(); err == nil; k, err = this.NextChar() {
		if k == '%' {
			nk, err = this.NextChar()
			if err != nil {
				break
			}
			if nk == '}' {
				break
			}
		}
	}
}

const template = `
<body>
<p>{{ greeting }}, {{ name }}!</p>
</body>
`

func main() {
	stash := map[string]string{
		"greeting": "Hello",
		"name":     "World",
	}
	tr := &TwigReader{
		content: bytes.NewReader([]byte(template)),
		tl:      make([]*TwigToken, 0, 8),
		stash:   stash,
	}
	tr.ParseTemplate()

	for _, k := range tr.tl {
		switch k.t {
		case 't':
			fmt.Print(k.value.(string))
		case 'v':
			key := k.value.(string)
			fmt.Print(tr.stash[key])
		}
	}
}
