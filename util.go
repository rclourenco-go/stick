package stick

func unrollValue(in interface{}) interface{} {
	switch x := in.(type) {
	case map[string]Value:
		out := make(map[string]interface{})
		for k, v := range x {
			out[k] = v.(interface{})
		}
		return out
	case map[string]interface{}:
		out := make(map[string]interface{})
		for k, v := range x {
			out[k] = unrollValue(v)
		}
		return out
	case []Value:
		out := make([]interface{}, len(x))
		for i, _ := range x {
			out[i] = x[i].(interface{})
		}
		return out
	case []interface{}:
		out := make([]interface{}, len(x))
		for i, _ := range x {
			out[i] = unrollValue(x[i])
		}
		return out
	case nil:
		return ""
	}

	return in
}
